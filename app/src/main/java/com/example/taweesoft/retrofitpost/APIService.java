package com.example.taweesoft.retrofitpost;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by TAWEESOFT on 4/18/16 AD.
 */
public interface APIService {

    @POST("searchBar")
    Call<Item> searchBarByName(@Body RequestBody requestBody);

}
