package com.example.taweesoft.retrofitpost;

import com.google.gson.annotations.SerializedName;

/**
 * Created by TAWEESOFT on 4/18/16 AD.
 */
public class Bar {
    @SerializedName("name") private String name;
    @SerializedName("description") private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
