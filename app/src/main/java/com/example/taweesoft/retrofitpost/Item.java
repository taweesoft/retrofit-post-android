package com.example.taweesoft.retrofitpost;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by TAWEESOFT on 4/18/16 AD.
 */
public class Item {
    @SerializedName("bars")
    List<Bar> barList;

    public List<Bar> getBarList() {
        return barList;
    }

    public void setBarList(List<Bar> barList) {
        this.barList = barList;
    }
}
